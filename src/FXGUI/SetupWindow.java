/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FXGUI;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.net.ssl.HttpsURLConnection;
import javax.swing.JOptionPane;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author finnetrolle
 */
public class SetupWindow {
    
    private Stage stage = null;
    private Stage parentStage = null;
    private AnchorPane page = null;
    
    public SetupWindow(Stage parent) {
        this.parentStage = parent;
        
        // create pane
        stage = new Stage();
        page = new AnchorPane();
        Scene scene = new Scene(page);
        
        GridPane grid = new GridPane();
        Label lab;
        lab = new Label("Enter your keyId");
        grid.add(lab, 0, 0);
        lab = new Label("Enter your vCode");
        grid.add(lab, 0, 1);
        lab = new Label("Select character");
        grid.add(lab, 0, 3);
        
        final TextField keyField = new TextField();
        grid.add(keyField, 1, 0);
        
        final TextField vCodeField = new TextField();
        grid.add(vCodeField, 1, 1);
        
        final Button b1 = new Button("Check API and load characters");
        grid.add(b1, 1, 2);
        
        final ComboBox<String> listCombo = new ComboBox();
        listCombo.setPrefWidth(250);
        listCombo.setMinWidth(250);
        listCombo.setMaxWidth(Double.MIN_VALUE);
        grid.add(listCombo, 1, 3);
        
        final Button b2 = new Button("Confirm");
        grid.add(b2, 1, 4);
        
        b1.setOnAction(new EventHandler<ActionEvent> () {

            @Override
            public void handle(ActionEvent event) {
                fillComboBox(keyField.getText(), vCodeField.getText(), listCombo);
            }
        });
        
        b2.setOnAction(new EventHandler<ActionEvent> () {

            @Override
            public void handle(ActionEvent event) {
                File config = new File("settings.txt");
                Properties props = new Properties();

                try {
                    if (config.exists()) {
                        config.delete();
                    }
                    OutputStream os = new FileOutputStream(config);
                    props.setProperty("host", "http://188.226.222.97:8080");
                    props.setProperty("keyId", keyField.getText());
                    props.setProperty("vCode", vCodeField.getText());
                    props.setProperty("character", listCombo.getSelectionModel().getSelectedItem());
                    props.store(os, "comments is useless");
                    os.close();
                    JOptionPane.showMessageDialog(null, "Settings file saved. Restart application");
                    stage.close();
                }
                catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "We really can't even save settings file. What the heck with your PC?");
                    stage.close();
                }
            }
            
        });
        
        page.getChildren().add(grid);
        stage.setScene(scene);
        stage.setTitle("Enter your EVE APIkey");
        stage.initOwner(parent);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.show();
        
    }
    
    public void fillComboBox(String apiKey, String vCode, ComboBox box) {
        try {
            
            String url = "https://api.eveonline.com/account/Characters.xml.aspx ";
            URL obj = new URL(url);
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
                    //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            StringBuilder parambuilder = new StringBuilder();
            parambuilder.append("keyID=");
            parambuilder.append(apiKey);
            parambuilder.append("&vCode=");
            parambuilder.append(vCode);
            
            String urlParameters = parambuilder.toString();
            
            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
            }
            in.close();
            
            XMLReader xr = XMLReaderFactory.createXMLReader();
            AuthXmlParser ps = new AuthXmlParser();
            xr.setContentHandler(ps);
            xr.setErrorHandler(ps);
            xr.parse(new InputSource(new StringReader(response.toString())));
            
            ArrayList<String> strlist = ps.getCharnames();
            for (int i = 0; i < strlist.size(); ++i) {
                box.getItems().add(strlist.get(i));
            }
            box.getSelectionModel().selectFirst();
            
            if (strlist.size() ==0)
                JOptionPane.showMessageDialog(null, "Your APIkey is shit. Try again");
                
            
        } catch (Exception e) {
        }
    }
    
}
