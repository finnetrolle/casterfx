/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FXGUI;

import frontend.xixalertMessages.CommandMessage.BroadcastList;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import xixAlertModel.xixAlertCore;

/**
 *
 * @author finnetrolle
 */
public class SendWindow {

    private xixAlertCore core = null;
    private int preselectedListId = -1;
    private Stage stage = null;
    private Stage parentStage = null;
    private AnchorPane page = null;
    
    public SendWindow(xixAlertCore core, int preselectedListId, Stage parent) {
        this.parentStage = parent;
        this.core = core;
        this.preselectedListId = preselectedListId;
        
        // create pane
        stage = new Stage();
        page = new AnchorPane();
        Scene scene = new Scene(page);
        
        GridPane grid = new GridPane();
        Label lab;
        lab = new Label("Broadcast List");
        grid.add(lab, 0, 0);
        lab = new Label("Caption");
        grid.add(lab, 0, 1);
        lab = new Label("Text");
        grid.add(lab, 0, 2);
        
        final ComboBox<String> listCombo = new ComboBox();
        final ArrayList<BroadcastList> lists = core.getModel().getLists();
        for (int i = 0; i < lists.size(); ++i) {
            BroadcastList list = lists.get(i);
            if (list.getCansend()) {
                listCombo.getItems().add(list.getName());
                if (list.getId() == preselectedListId)
                    listCombo.getSelectionModel().selectLast();
            }
        }

        grid.add(listCombo, 1, 0);
        
        final TextField tf = new TextField();
        grid.add(tf, 1, 1);
        
        final TextArea ta = new TextArea();
        grid.add(ta, 1, 2);
        
        final Button b1 = new Button("Send");
        final Button b2 = new Button("Cancel");
        b2.setCancelButton(true);
        GridPane subgrid = new GridPane();
        subgrid.add(b1, 1, 0);
        subgrid.add(b2, 0, 0);
        grid.add(subgrid, 1,3);
        
        b1.setOnAction(new EventHandler<ActionEvent> () {

            @Override
            public void handle(ActionEvent event) {
                String s = listCombo.getSelectionModel().getSelectedItem();
                int id = -1;
                for (int i = 0; i < lists.size(); ++i) {
                    BroadcastList list = lists.get(i);
                    if (list.getName().equals(s)) {
                        id = list.getId();
                    }
                }
                try {
                    core.sendMessage(tf.getText(), ta.getText(), id);
                    JOptionPane.showMessageDialog(null, "Message sent");
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Error while sending "+ e.toString());
                }
                stage.close();
            }
        });
        
        b2.setOnAction(new EventHandler<ActionEvent> () {

            @Override
            public void handle(ActionEvent event) {
                stage.close();
            }
            
        });
        
        page.getChildren().add(grid);
        stage.setScene(scene);
        stage.setTitle("Send message");
        stage.initOwner(parent);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.show();
        
    }
    
    
    
}
