/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FXGUI;

import frontend.xixalertMessages.CommandMessage.BroadcastList;
import frontend.xixalertMessages.CommandMessage.BroadcastMessage;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import xixAlertModel.xixAlertCore;

/**
 *
 * @author finnetrolle
 */
public class BroadcastTab extends Tab{
    
    private BroadcastList list = null;
    
    final private WebView webView;
    final private xixAlertCore core;
    final private Stage parentStage;
    
    public void update() {
        if (core == null)
            return;
        ArrayList<BroadcastMessage> msgs = 
                core.getModel().getMessagesForList(list.getId());
        String bgcolor1 = "#FBF8EF";
        String bgcolor2 = "#FFFFFF";
        String actualColor = null;
        StringBuilder sb = new StringBuilder();
        for (int i = msgs.size() - 1; i >= 0; --i) {
            BroadcastMessage msg = msgs.get(i);
            if (i % 2 == 0) 
                actualColor = bgcolor1;
            else
                actualColor = bgcolor2;
            sb.append(createHTMLFromMessage(msg, actualColor));
        }
        webView.getEngine().loadContent(sb.toString());
    }
    
    public int getListId() {
        return this.list.getId();
    }
    
    private String createHTMLFromMessage(BroadcastMessage msg, String bgcolor) {
        StringBuilder sb = new StringBuilder();
        sb.append("<table width = 100% border = 0 bgcolor = ");
        sb.append(bgcolor);
        sb.append(" cellpadding = 3 cellspacing = 3 border = 0>");
        sb.append("<tr><td align = left><b>[");
        sb.append(msg.getSendTime());
        sb.append("]</b></td><td align = left><b>");
        sb.append(msg.getMessageCaption());
        sb.append("</b></td><td align = right> from <b>");
        sb.append(msg.getSender());
        sb.append("</b></td></tr><tr><td>&nbsp;</td><td colspan = 2 align = justify width = 100%>");
        sb.append(msg.getMessageText());
        sb.append("</td></tr></table>");
        return sb.toString();
    }
    
    public BroadcastTab(Stage parentStage, BroadcastList list, xixAlertCore core) {
        super(list.getName());
        this.core = core;
        this.webView = new WebView();
        this.list = list;
        this.parentStage = parentStage;
        
        this.setStyle("-fx-font: 10pt courier; ");
        this.setText(list.getName());
        
        webView.setFontScale(0.8);
        webView.setMaxHeight(Double.MAX_VALUE);
        webView.setMinSize(250, 250);
        
        VBox vBox = new VBox(2);
        vBox.getChildren().add(webView);
        VBox.setVgrow(webView, Priority.ALWAYS);
        
        if (list.getCansend()) {
            Button btn = new Button("Send message to this list");
            btn.setOnAction(new EventHandler<ActionEvent>() {

                @Override
                public void handle(ActionEvent event) {
                    try {
                        //SendWindow sw = new SendWindow(api, wlists, list.getId(), parentStage);
                        SendWindow sw = new SendWindow(core, list.getId(), parentStage);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            btn.setMaxWidth(Double.MAX_VALUE);
            vBox.getChildren().add(btn);
        }
        
        this.setContent(vBox);
        this.setClosable(false);
    }
    
    
    
    
}
