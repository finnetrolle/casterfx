/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FXGUI;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author finnetrolle
 */
public class AuthXmlParser extends DefaultHandler{
    
    
    public AuthXmlParser() {
        super();
        
    }
    
    private ArrayList<String> charnames = new ArrayList<>();

    public ArrayList<String> getCharnames() {
        return charnames;
    }
    
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (localName.equals("row")) {
            // read row
            charnames.add(attributes.getValue("name").toString());
        }
    }
}
