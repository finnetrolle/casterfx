/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package casterfx;

import FXGUI.BroadcastTab;
import FXGUI.SetupWindow;
import frontend.xixalertMessages.CommandMessage.BroadcastList;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import static javafx.scene.input.DataFormat.URL;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import xixAlertModel.AlertModel;
import xixAlertModel.xixAlertCore;

/**
 *
 * @author finnetrolle
 */
public class CasterFX extends Application {
    
    private xixAlertCore core;
    private String charname = null;
    private String keyId = null;
    private String vCode = null;
    private String hostname = null;
    private String appName = "CasterFX v 0.2a";

    TabPane tabPane = new TabPane();
    
    private ArrayList<BroadcastTab> tabs = null;
    
    @Override
    public void start(Stage primaryStage) {
        final Stage myStage = primaryStage;
        // read settings file
        File config = new File("settings.txt");
        Properties props = new Properties();
        try {
            if (config.exists() == false) {
                SetupWindow wdw = new SetupWindow(primaryStage);
                return;
            }
            InputStream is = new FileInputStream(config);
            props.load(is);
            charname = props.getProperty("character");
            keyId = props.getProperty("keyId");
            vCode = props.getProperty("vCode");
            hostname = props.getProperty("host");
        }
        catch (Exception e) {
            SetupWindow wdw = new SetupWindow(primaryStage);
            return;
        }
        
        // now we have settings and init core
        core = new xixAlertCore(hostname, 2, 50);
        core.setUserAuthInfo(charname, keyId, vCode);
        tabs = new ArrayList<>();
        
        // create core handlers
        core.setModelBroadcastListsUpdated(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                ArrayList<BroadcastList> lists = core.getModel().getLists();
                for (int i = 0; i < lists.size(); ++i) {
                    BroadcastList list = lists.get(i);
                    boolean createNew = true;
                    for (int j = 0; j < tabs.size(); ++j) {
                        BroadcastTab tab = tabs.get(j);
                        if (tab.getListId() == list.getId())
                            createNew = false;
                    }
                    if (createNew) {
                        BroadcastTab tab = null;
                        try {
                            tab = new BroadcastTab(myStage, list, core);
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                            ex.printStackTrace();
                        }
                        tabs.add(tab);
                        tabPane.getTabs().add(tab);
                    }
                }
            }
        } );
        core.setModelMessagesUpdated(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                for (int i = 0; i < tabs.size(); ++i) {
                    BroadcastTab tab = tabs.get(i);
                    tab.update();
                }
                URL v = getClass().getResource("sound.wav");
                Media hit = new Media(v.toString());
                MediaPlayer mediaPlayer = new MediaPlayer(hit);
                mediaPlayer.play();
            }
        });
        core.setModelServerInfoUpdated(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
//                if (!core.isConnectionEstablished())
//                    myStage.setTitle("AlertFX for " + core.getModel().getCharname() + " is offline");
            }
        });
        core.setModelUserAuthInfoUpdated(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                AlertModel.AuthStatus status = core.getModel().getAuthStatus();
                switch (status) {
                    case AUTHREQUIRED: myStage.setTitle(appName + " for unknown user"); break;
                    case WAITINGFORAUTH: myStage.setTitle(appName + " waiting for auth"); break;
                    case AUTHSUCCESS: myStage.setTitle(appName + " for " + core.getModel().getCharname()); break;
                    case AUTHDENIED: myStage.setTitle(appName); 
                                     JOptionPane.showMessageDialog(null, "Auth failed - edit your settings data"); break;
                }
            }
        });
                 
        
        // start core
        //core.stopAndStart();
        
        // start My Own timers
        Timer pingTimer = new java.util.Timer(true);
        pingTimer.schedule(new TimerTask() {
            public void run() {
                 Platform.runLater(new Runnable() {
                    public void run() {
                        core.engage();
                    }
                });
            }
        }, 500, 500);
        
        Timer askTimer = new java.util.Timer(true);
        askTimer.schedule(new TimerTask() {
            public void run() {
                 Platform.runLater(new Runnable() {
                    public void run() {
                        core.askLists();
                        core.askMessages();
                    }
                });
            }
        }, 2000, 5000);
        
        Button sendButton = new Button("Send message");
        Button settingsButton = new Button("Settings");
        sendButton.addEventHandler(EventType.ROOT, new EventHandler() {

            @Override
            public void handle(Event event) {
                
            }
        });
        
        
        ToolBar bar = new ToolBar(sendButton, settingsButton);
        
        StackPane root = new StackPane();
        VBox box = new VBox(2);
        //box.getChildren().add(bar);
        box.getChildren().add(tabPane);
        root.getChildren().add(box);
        Scene scene = new Scene(root, 640, 480);
        primaryStage.getIcons().add(new Image("/FXGUI/msg.png"));
        primaryStage.setMinHeight(480);
        primaryStage.setMinWidth(640);
        primaryStage.setScene(scene);
        primaryStage.setTitle(appName + " for " + charname);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
